% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/waterContent.R
\name{waterContent}
\alias{waterContent}
\alias{waterContent,CrnsData,numeric-method}
\title{conversion of corrected neutron count rates to gravimetric water content and optionally to volumetric water content}
\usage{
waterContent(CrnsData, corType, ...)

\S4method{waterContent}{CrnsData,numeric}(
  CrnsData,
  corType = 4,
  a0 = 0.0808,
  a1 = 0.372,
  a2 = 0.115,
  bd = NA,
  lw = 0,
  ow = 0,
  cntrange = NA
)
}
\arguments{
\item{CrnsData}{a CrnsData object}

\item{corType}{type of neutron correction to be applied prior to water content computation}

\item{a0}{equation parameter}

\item{a1}{equation parameter}

\item{a2}{equation parameter}

\item{bd}{observed value for soil bulk density (g/cm^3) to convert gravimetric to volumetric water content}

\item{lw}{lattice water (g/g)}

\item{ow}{organic water (g/g)}

\item{cntrange}{bounds for syntetic counts computation}
}
\value{
Returns a CrnsData object with updated gravimetric and optionally volumetric water content.
}
\description{
conversion of corrected neutron count rates to gravimetric water content and optionally to volumetric water content
}
\references{
CRS-1000 User's Guide/rev 004-1, Desiltes et al. 2010, Zreda et al. 2012
}
\author{
Alexander Raphael Groos, Benjamin Fersch
}
